## Welcome to Dataday 2022 workshop - Analytics part

We will look into one real AB test that we did on our search page - we will explore what was the goal, how it actually looked like (UI), what are the key metrics we wanted to influence. Before we go into the actual evaluation of the AB test using SQL, we will explore the underlying data.

We chose AB test evaluation for this workshop as experimentation is integral part of Product-oriented and Data-driven organisation such as Kiwi.com is. We conclude all the product-related decisions based on experimentation and data.


### Context
As probably all of the companies, we also understand that knowing our customers benefits both sides - we can offer more personalised and seamless experience which improves retention, customer satisfaction and in the end, brings higher revenue.

One of the most straightforward options to effectively achieve this is when customers have an account and are signed in during their sessions. Therefore we decided to conclude series of experiments in order to increase the number of signed in customers.

### Experiment set up

#### Goal
The goal of the experiment is to explore whether and how we can increase the number of signed in users, without causing a major disruption to the customer experience. We don't want to loose customers for the sake of higher number of signed in customers.

#### UX Design
A new modal is displayed inviting the customer to sign in with promoting the benefits - faster booking, price alert set up and trips overview.
<div align="center">
  <img alt="active shell" src="imgs/test_design.png" width="75%"/>
</div>

#### Key metrics
- **Number of signed in customers**
  - desired impact: increase
  - for the sake of simplicity, we will look at what is the % share of people clicking *Sign in* button
- **Search Conversion Rate (sCR)**
  - desired impact: not harm
  - we define sCR as ratio of unique customers (users) who finished the booking out of all unique customers who were exposed to search results

### Workshop guide
Here are the necessary steps to explore the data set and evaluate the results. As with any analytical task, there are many ways how to achieve the final result and many extra steps that one can take in order to discover more insights. Please feel encouraged to play with the data set and explore it to the fullest!

#### 1. Explore the data
We have two tables - `event` and `booking`.

**Event** table contains selected frontend events relevant for this workshop. Event is an action that happens on our product - it can be user-invoked action as button click or input entering, or a product-invoked action such as background checks, API-calls etc.

**Booking** table contains list of confirmed bookings, that we can join to the users from `event` table to understand what customers converted.

Let's look at what is in the tables, starting with `event` table:

<details>
    <summary>basic exploration of event table:</summary>

```sql
SELECT *
  FROM dataday_extended.event
```
</details>

There are few key columns:
- `visitorId`: id of our visitor (=customer), same for the whole interaction with our product
- `bid`: Booking ID - for connection with `booking` table
- `module`, `category`, `action`: identification of particular events
- `test_name`: name of the AB test
- `test_group`: group of the AB test (control vs treatment, on vs off), representing what version of our product was displayed to the visitor

Let's see what different `test_group` values are in our data:

<details>
    <summary>summary of the test_group column:</summary>

```sql
SELECT test_group
      ,count(*) as number_of_events
  FROM dataday_extended.event
 GROUP BY test_group
 ORDER BY count(*) DESC
```
</details>

<div align="center">
  <img alt="active shell" src="imgs/test_group_summary.png" width="75%"/>
</div>

We see that the majority of events have value `on` or `off` in the `test_group` column. There is only a few `disabled` values, representing less than 0.1% of all events. We can ignore them in our next steps.
What other columns are worth exploring? Take some time to check a few of them in a similar manner and let us know what you found!

As you have probably already noticed, we have different events in the `event` table, so let's see what they are.

<details>
    <summary>checking the events:</summary>

```sql
SELECT platform
      ,module
      ,category
      ,action
      ,count(*) as count
  FROM dataday_extended.event
  GROUP BY 1,2,3,4
  ORDER BY count(*) DESC
```
</details>

<div align="center">
  <img alt="active shell" src="imgs/events_overview.png" width="75%"/>
</div>

There is quite a big number of events in our table, but we will use only a few of them:
- **module = 'search' AND category = 'result list' AND action = 'load results'**: search result were displayed - we also call it search started
- **module = 'search' AND category = 'login' AND action = 'modal shown'**: log in modal was displayed
- **module = 'search' AND category = 'login' AND action = 'required login continue as guest button click'**: customer clicked on the "continue as guest" button
- **module = 'search' AND category = 'login' AND action = 'required login sign in button click'**: customer clicked on the "sign in" button
- **module = 'booking' AND category = 'general' AND action = 'started'**: customer proceeded with selected itinerary to the beginning of the booking funnel


Now let's check the `booking` table.

<details>
    <summary>basic exploration of booking table:</summary>

```sql
SELECT *
  FROM dataday_extended.booking
```
</details>

This table is pretty simple, contains just a few columns, `bid` - Booking ID - being the most important for us as it will allow us to connect with the events.


#### 2. Check the Data Quality

Data Quality is maybe not very popular, but extremely important topic. Without high-quality data, we can't produce high-quality results!
Even though we have very talented Software Engineers, Data Engineers and Data Analysts, it is always better to check if the data is in perfect shape. Let's do two basic checks (you can try to do it themselves, or just follow the code below):

**One user is assigned to only one AB** `test_group` - if the user is not assigned to only one test group, we have a problem to decide into which part of the experiment does he belong and how was he affected by the strange behaviour	of our product, seeing two different versions of the same product feature.

<details>
    <summary>`test_group` uniqueness DQ check:</summary>

```sql
SELECT visitorid,
        COUNT(*) AS number_of_versions
  FROM (
        SELECT visitorid,
              test_group as AB_group
          FROM dataday_extended.event
        WHERE test_name = 'SIGN_IN_BEFORE_BOOKING'
        GROUP BY
              visitorid,
              AB_group
       )
 GROUP BY visitorid
HAVING COUNT(*) > 1
```
</details>

We see that there is a small amount of users who have more than one group. That is not the best news, and we explored it with our Software Engineers. It turns out it is expected edge-case behaviour and we can just ignore them in our further evaluation.

**The bids in `booking` table are unique, there are no duplicities** - when joining tables, we need to understand if the column used for joining contains duplicities or not and whether they are desired or not. Otherwise, we might get some unexpected results.

<details>
    <summary>`bid` uniqueness DQ check:</summary>

```sql
SELECT count(*) as total_rows
      ,count(DISTINCT bid) as unique_bids
  FROM dataday_extended.booking
```
</details>

Unfortunately, there is a significant difference between total number of rows and unique bids, meaning there is a lot of duplicate values in the `bid` column. Good news - it is not a problem, since we can solve it in the code during the evaluation.


#### 3. Evaluate the AB test

For evaluation of an AB test, we want to first clarify what and where we want to measure. Good practice is always to measure only the smallest relevant part of customer actions that is influenced by the test - we don't want to have some external factors - noise - skew the results.
In our case, the first step where customers are possibly impacted by the AB test, is the page with displayed search results. This is where the modal can be displayed. The final step is when we confirm the booking - meaning customer has successfully paid and we have issued the booking. With these two actions, we can already create the basic funnel, that will tell us how was the conversion impacted by the test group.
However we want to also evaluate how many customers actually signed in thanks to our new sign in modal, so that action is going to play a role in our evaluation as well.

For the beginning, let's start with the first step - page with search results was displayed to the customer. Lets extract list of visitors in this step and name of the `test_group` they were assigned into.

<details>
    <summary>search results displayed:</summary>

```sql
SELECT visitorid
      ,MAX(test_group) AS AB_group
  FROM dataday_extended.event
  WHERE module = 'search'
        AND category = 'result list'
        AND action = 'load results'
        AND test_name = 'SIGN_IN_BEFORE_BOOKING'
        AND test_group IN ('on','off')
  GROUP BY 1
```
</details>

Now let's get the last step of our funnel, showing us bookings that the customers made. We are going to join `event` and `booking` table using common column `bid`. We use our finding from DQ check and make sure that duplicated bids are not going to multiply the number of rows after the join.

<details>
    <summary>bookings confirmed:</summary>

```sql
SELECT visitorId,
       bid,
       MAX(expected_net_revenue) AS expected_net_revenue,
  FROM dataday_extended.event fro
       JOIN dataday_extended.booking bkn
       USING(bid)
  GROUP BY 1,2
```
</details>

Great, we have the two main steps of the funnel! Let's connect them together and see what it shows us. We will use Common Table Expressions (CTEs) in order to make our code more structured and readable:

<details>
    <summary>basic funnel:</summary>

```sql
WITH search_started AS ( 
  SELECT visitorid
        ,MAX(test_group) AS AB_group
    FROM dataday_extended.event
   WHERE module = 'search'
         AND category = 'result list'
         AND action = 'load results'
         AND test_name = 'SIGN_IN_BEFORE_BOOKING'
         AND os IN ('Windows', 'Mac OS X','Android','iOS')
         AND test_group IN ('on','off')
   GROUP BY 1
),

confirmed_bookings AS(
    SELECT visitorId,
           bid,
           MAX(expected_net_revenue) AS expected_net_revenue,
      FROM dataday_extended.event fro
           JOIN dataday_extended.booking bkn 
           USING(bid)
     GROUP BY 1,2
)

SELECT AB_group,
       COUNT(DISTINCT sst.visitorid) AS search_started_cookies,
       COUNT(DISTINCT cb.visitorid) AS bookings
  FROM search_started AS sst
       LEFT JOIN confirmed_bookings as cb
              ON sst.visitorId = cb.visitorId
 GROUP BY 1
 ORDER BY 1
```
</details>

The first look into our funnel looks like this:

<div align="center">
  <img alt="active shell" src="imgs/basic_funnel.png" width="75%"/>
</div>

It shows us number of unique customers that were in the first step - search results displayed - and made it to confirmed booking.

Now it's time to remember the other DQ check - users exposed to both groups of the AB test. We add a special check to identify those users and then just remove them from the evaluation.

<details>
    <summary>filtering out users exposed to both test groups:</summary>

```sql
WITH search_started AS ( 
  SELECT visitorid
        ,MAX(test_group) AS AB_group
    FROM dataday_extended.event
   WHERE module = 'search'
         AND category = 'result list'
         AND action = 'load results'
         AND test_name = 'SIGN_IN_BEFORE_BOOKING'
         AND os IN ('Windows', 'Mac OS X','Android','iOS')
         AND test_group IN ('on','off')
   GROUP BY 1
),

confirmed_bookings AS(
    SELECT visitorId,
           bid,
           MAX(expected_net_revenue) AS expected_net_revenue,
      FROM dataday_extended.event fro
           JOIN dataday_extended.booking bkn -- INNER, ONLY WHERE BOTH ARE IN 
           USING(bid)
     GROUP BY 1,2
),

duplicity_check AS(
    SELECT visitorid,
           COUNT(*) AS number_of_versions
      FROM(
           SELECT visitorid,
                  test_group as AB_group
             FROM dataday_extended.event
            WHERE module = 'search'
                  AND category = 'result list'
                  AND action = 'load results'
                  AND test_name = 'SIGN_IN_BEFORE_BOOKING'
                  AND os IN ('Windows', 'Mac OS X','Android','iOS')
                  AND test_group IN ('on','off')
            GROUP BY
                  visitorid,
                  AB_group
          )
     GROUP BY 
           visitorid
)

SELECT AB_group,
       COUNT(DISTINCT sst.visitorid) AS search_started_cookies,
       COUNT(DISTINCT cb.visitorid) AS bookings
  FROM search_started AS sst
       LEFT JOIN confirmed_bookings as cb
              ON sst.visitorId = cb.visitorId
       LEFT JOIN duplicity_check as dc
              ON sst.visitorId = dc.visitorId
 WHERE dc.number_of_versions = 1
 GROUP BY 1
 ORDER BY 1
```
</details>

As a final step of building the SQL script, we will add some more steps to our funnel: display of sign in modal, click on "continue as guest" and "sign in" on the modal and start of booking event. The final SQL is below:

<details>
    <summary>final evaluation sql:</summary>

```sql
WITH search_started AS ( 
  SELECT visitorid
        ,MAX(test_group) AS AB_group
    FROM dataday_extended.event
   WHERE module = 'search'
         AND category = 'result list'
         AND action = 'load results'
         AND test_name = 'SIGN_IN_BEFORE_BOOKING'
         AND os IN ('Windows', 'Mac OS X','Android','iOS')
         AND test_group IN ('on','off')
   GROUP BY 1
),

modal_shown AS (
  SELECT visitorId
    FROM dataday_extended.event
   WHERE module = 'search'
         AND category = 'login'
         AND action = 'modal shown'
   GROUP BY 1
),

continue_as_guest AS (
  SELECT visitorId
    FROM dataday_extended.event
   WHERE module = 'search'
         AND category = 'login'
         AND action = 'required login continue as guest button click'
   GROUP BY 1
),

sign_in_clicked AS (
  SELECT visitorId
    FROM dataday_extended.event
   WHERE module = 'search'
         AND category = 'login'
         AND action = 'required login sign in button click'
   GROUP BY 1
),

booking_started AS ( 
  SELECT visitorid
    FROM dataday_extended.event
   WHERE module = 'booking'
         AND category = 'general'
         AND action = 'started'
   GROUP BY 1
),

confirmed_bookings AS(
    SELECT visitorId,
           bid,
           MAX(expected_net_revenue) AS expected_net_revenue,
      FROM dataday_extended.event fro
           JOIN dataday_extended.booking bkn 
           USING(bid)
     GROUP BY 1,2
),

duplicity_check AS(
    SELECT visitorid,
           COUNT(*) AS number_of_versions
      FROM(
           SELECT visitorid,
                  test_group as AB_group
             FROM dataday_extended.event
            WHERE module = 'search'
                  AND category = 'result list'
                  AND action = 'load results'
                  AND test_name = 'SIGN_IN_BEFORE_BOOKING'
                  AND os IN ('Windows', 'Mac OS X','Android','iOS')
                  AND test_group IN ('on','off')
            GROUP BY
                  visitorid,
                  AB_group
          )
     GROUP BY 
           visitorid
)

SELECT AB_group,
       COUNT(DISTINCT sst.visitorid) AS search_started,
       COUNT(DISTINCT ms.visitorid) AS modal_shown,
       COUNT(DISTINCT cag.visitorid) AS continue_as_guest,
       COUNT(DISTINCT sic.visitorid) AS sign_in_clicked,
       COUNT(DISTINCT bst.visitorid) AS booking_started,
       COUNT(DISTINCT cb.visitorid) AS bookings
  FROM search_started AS sst
       LEFT JOIN modal_shown as ms
              ON sst.visitorId = ms.visitorId
       LEFT JOIN continue_as_guest as cag
              ON sst.visitorId = cag.visitorId
       LEFT JOIN sign_in_clicked as sic
              ON sst.visitorId = sic.visitorId
       LEFT JOIN booking_started as bst
              ON sst.visitorId = bst.visitorId
       LEFT JOIN confirmed_bookings as cb
              ON sst.visitorId = cb.visitorId
       LEFT JOIN duplicity_check as dc
              ON sst.visitorId = dc.visitorId
 WHERE dc.number_of_versions = 1
 GROUP BY 1
 ORDER BY 1
```
</details>

Result of the complete funnel is here:

<div align="center">
  <img alt="active shell" src="imgs/final_funnel.png" width="75%"/>
</div>

There are few interesting insights:
- number of modal_shown events: we would expect 0 for the "off" group, because the modal should not be shown there. While there is more than 5000 events tracked for this group, we see it's significantly lower compared to the "on" group, hinting it might be some bug in tracking. This hypothesis is strongly supported by the following two events - `continue_as_guest` and `sign_in_clicked`, where we see almost no users for the "off" group.
- the overall sCR (search Conversion Rate - between search_started and bookings) is 8.8% for "off" group and slightly lower, 8.4% for the "on" group
- while the sCR is pretty similar for both groups, we see that the difference in `booking_started` event is higher, around 7% less people come to this step. That means people in the treatment - "on" group - are more likely to drop off because of the sign in modal, however they are more likely then to finish the booking
- we see that 4378 people decided to sign in thanks to the sign in modal, which is 15.4% of those who saw the sign in modal. That is a nice results!

To evaluate the significance of sCR impact, we use Wald intervals. With code in R, we calculate if the change between sCR for both groups is significant or not:
```
install.packages("PropCIs")
install.packages("binom")
library(PropCIs)
library(binom)

# diffscoreci(Test{success, total,} Control{success, total,} conf.level=0.95)
diffscoreci(1818,216475,1887,215421, conf.level=0.95)
```

Running this piece of R script, we get the Wald interval for the difference between the search Conversion Rates (-0.09pp, +0.02pp), meaning that the real difference lies with 95% confidence somewhere in this interval. Since we have 0 inside the interval, we don't conclude the impact of the AB test on the sCR as significant.


#### The AB test evaluation part is done, congrats! Now let's deep dive with python into the data to discover some deeper insights.
